
function createCard(title, description, pictureUrl, start, ends, location) {
    return `
    <div class="col-4">
        <div class="card" aria-hidden="true">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title placeholder-glow">
                <span class="placeholder">${title}</span>
                </h5>
                <h6 class="card-subtitle mb-2 text-muted">
                <span class="placeholder ">${location}</span>
                </h6>
                <p class="card-text placeholder-glow">
                <span class="placeholder">${description}</span>
                </p>
            </div>
            <div class="card-footer">
                <small class="text-muted placeholder-glow">
                <span class="placeholder">${start}-${ends}</span>
                </small>
            </div>
        </div>
    </div>
    `;
  }

function createAlert(){
    return `
    <div class="alert alert-danger" role="alert"> A simple danger alert—check it out! </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);

        if (!response.ok){
            const html = createAlert();
            const column = document.querySelector('.row');
            column.innerHTML += html;
        } else {
            const data = await response.json();

            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;

                    const startDate = new Date(details.conference.start);
                    const endsDate = new Date(details.conference.ends);

                    let start = startDate.toLocaleDateString();
                    let ends = endsDate.toLocaleDateString();

                    const html = createCard(title, description, pictureUrl, start, ends, location);

                    const column = document.querySelector('.row');
                    column.innerHTML += html;

                }
            }

        const card = document.querySelectorAll('.placeholder');
            card.forEach(checkcard =>{
                checkcard.classList.remove('placeholder');
            });

        }
    } catch (e) {
        //Figure out what to do if an error is raised
        console.error(e);
    }

});
